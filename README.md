
<!-- README.md is generated from README.Rmd. Please edit that file -->
[![Build status](https://gitlab.com/artemklevtsov/OpenDotaExplorer/badges/master/pipeline.svg)](https://gitlab.com/artemklevtsov/OpenDotaExplorer/commits/master) [![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

OpenDotaExplorer
================

The R package to fetch SQL query from the OpenDota Explorer.

Installation
------------

To install the development version the following command can be used:

``` r
install.packages("OpenDotaExplorer", repos = "https://artemklevtsov.gitlab.io/OpenDotaExplorer/drat")
```

Usage
-----

First we need to load the package:

``` r
library(OpenDotaExplorer)
```

Get heroes:

``` r
query_opendota_explorer(paste(
    "SELECT id, localized_name, primary_attr, attack_type",
    "FROM heroes",
    "ORDER BY id LIMIT 5"
))
#>   id localized_name primary_attr attack_type
#> 1  1      Anti-Mage          agi       Melee
#> 2  2            Axe          str       Melee
#> 3  3           Bane          int      Ranged
#> 4  4    Bloodseeker          agi       Melee
#> 5  5 Crystal Maiden          int      Ranged
```

Get The International tournaments IDs:

``` r
query_opendota_explorer(paste(
    "SELECT leagueid, name",
    "FROM leagues",
    "WHERE name LIKE 'The International%'"
))
#>    leagueid                                   name
#> 1      5498 The International 2017 Open Qualifiers
#> 2       600                 The International 2014
#> 3      2733                 The International 2015
#> 4      4768 The International 2016 Open Qualifiers
#> 5     65006            The International 2013 Pass
#> 6     65001                 The International 2012
#> 7     65005      The International West Qualifiers
#> 8     65004      The International East Qualifiers
#> 9      4664                 The International 2016
#> 10     5401                 The International 2017
```

Get the database schema:

``` r
query_opendota_explorer(paste(
    "SELECT DISTINCT table_name",
    "FROM information_schema.tables",
    "WHERE table_schema = 'public'",
    "ORDER BY table_name"
))
#>               table_name
#> 1       competitive_rank
#> 2                 heroes
#> 3           hero_ranking
#> 4                  items
#> 5                leagues
#> 6                matches
#> 7             match_logs
#> 8            match_patch
#> 9          mmr_estimates
#> 10       notable_players
#> 11            picks_bans
#> 12        player_matches
#> 13        public_matches
#> 14 public_player_matches
#> 15                 queue
#> 16 solo_competitive_rank
#> 17            team_match
#> 18           team_rating
#> 19                 teams
```

Get `picks_bans` table structure:

``` r
query_opendota_explorer(paste(
    "SELECT column_name, data_type",
    "FROM information_schema.columns",
    "WHERE table_schema = 'public'",
    "AND table_name = 'picks_bans'",
    "ORDER BY table_name"
))
#>   column_name data_type
#> 1    match_id    bigint
#> 2     is_pick   boolean
#> 3     hero_id   integer
#> 4        team  smallint
#> 5         ord  smallint
```

Bug reports
-----------

Use the following command to go to the page for bug report submissions:

``` r
bug.report(package = "OpenDotaExplorer")
```

Before reporting a bug or submitting an issue, please do the following:

-   Make sure that no error was found and corrected previously identified. You can use the search by the bug tracker;
-   Check the news list for the current version of the package. An error it might have been caused by changes in the package. This can be done with `news(package = "OpenDotaExplorer", Version == packageVersion("OpenDotaExplorer"))` command;
-   Make a minimal reproducible example of the code that consistently causes the error;
-   Make sure that the error triggered in the function from the `OpenDotaExplorer` package, rather than in the code that you pass, that is other functions or packages;
-   Try to reproduce the error with the last development version of the package from the git repository.

When submitting a bug report please include the output produced by functions `traceback()` and `sessionInfo()`. This may save a lot of time.

License
-------

The `OpenDotaExplorer` package is distributed under [GPLv3](https://www.gnu.org/licenses/gpl-3.0) license.
